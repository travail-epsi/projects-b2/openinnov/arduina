#include "main.h"

MFRC522 mfrc522(SS_PIN, RST_PIN);

AltSoftSerial BTserial;

void setup() {
    Serial.begin(9600);

    Serial.print("Sketch:   ");
    Serial.println(__FILE__);
    Serial.print("Uploaded: ");
    Serial.println(__DATE__);
    Serial.println(" ");
    BTserial.begin(9600);
    Serial.println("BTserial started at 9600");



    SPI.begin();
    mfrc522.PCD_Init();
    delay(4);
    mfrc522.PCD_DumpVersionToSerial();
    Serial.println("Scan PICC to see UID, SAK, type, and data blocks…");

}

void loop() {
    if ( ! mfrc522.PICC_IsNewCardPresent()) {
        return;
    }

    if ( ! mfrc522.PICC_ReadCardSerial()) {
        return ;
    }
    String ID = "";
    for (byte i = 0; i < mfrc522.uid.size; i++) {
        Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        Serial.print(mfrc522.uid.uidByte[i], HEX);
        ID.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
        ID.concat(String(mfrc522.uid.uidByte[i], HEX));
        delay(300);
    }
    Serial.println();

    if(ID.length() > 0 && ID.length() >= 11){
        ID.toUpperCase();
        Serial.print("ID: ");
        Serial.println(ID.c_str());
        BTserial.write(ID.c_str());
    }
}
